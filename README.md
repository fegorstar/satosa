#satosa container
Repository with directories and files to deploy a containerized SATOSA Proxy instance with docker-compose.
The container uses a SATOSA image based on the original source code with some changes.
This repository also has texts describing some characteristics of SATOSA and guides with the
 steps for implementation containing examples of configurations, according to the index presented below. All the contents of these documents 
were based on the wiki of the official SATOSA repository and adapted for the scenario of this instance.

#Index:

Features and Architecture.
Installation and Initial Setup.
Configuring Shibboleth SPs with SATOSA.


#Features
SATOSA is a proxy solution, maintained by the Identity Python organization, for 
translation and mapping of attributes between the main authentication protocols, 
such as: SAML2, OpenID Connect and OAuth2. These functionalities allow the integration 
of services with authentication mechanisms without the 
need to implement all protocols. 
More information at: Features and Architecture.

#Installation:
The following steps describe the deployment of a SATOSA instance with activation of Backend and Frontend Plugins 
for the SAML protocol. The configuration files present in the ./volumes directory of this repository present an 
example of a scenario using SATOSA as a SAML to 
SAML proxy where the trust relationship is established between an SP and a SAML Federation (idp) through the proxy.

#requirements
Server with valid FQDN or IP.
Private key and TLS certificate.
Docker Engine.
Docker Compose.

Initial Settings
1. Download the repository and access the root directory
 $ git clone https://git.rnp.br/gidlab/satosa-docker.git
 $ cd satosa-docker/

2. Change the variables in the .env and .satosa_env files to server information.
3. To enable TLS in Gunicorn itself, present in the docker image, insert files (private key and certificate) 
in the ./volumes/ directory, with the names https.key and https.crt.

4. Insert the private key and certificate files for signing and encrypting SAML assertions in the ./volumes/ directory, 
with the names metadata.key and metatada.crt. If you don't already have it, run the tutorial to generate it.

5. Change the frontend information (Proxy IdP) in the ./volumes/plugins/frontends/saml2_frontend.yaml file, 
especially the metadata property inside the idp_config key. Here, the metadata of the Service 
Providers (SPs) that will use the proxy to communicate with the federation must be informed.

6. Change backend (Proxy SP) information in the ./volumes/plugins/backends/saml2_backend.yaml file.
7. Change general settings information in the ./volumes/proxy_conf.yaml file.

Note: The files configure the proxy between CAFe Expresso Federation and SPs with the SAML protocol. 
See Plugins Configuration for other authentication mechanisms or more details.

start container
Run the command docker-compose up to follow the logs in the terminal or docker-compose up -d to start compose in deamon mode.


Trust relationship
As the SATOSA Proxy of this configuration has 2 SAML plugins enabled, it will be possible to get the frontend and backend metadata. 
If the entityid property has not been changed, the respective files can be accessed via the web at:

1. Metadata:

Frontend - https://FQDN/Saml2IDP/proxy.
Backend - https://FQDN/Saml2/proxy_saml2_backend.

Note: Change the FQDN to that of the server.


2. Configure the Frontend metadata on the SAML SP(s) configured in step 5 of the Initial Settings.

3. Forward the Backend metadata to the federation operator.


Attribute Mapping
The current files have the necessary settings for mapping all the attributes present in 
the user directories currently used in the CAFe Expresso Federation.
Altogether there are four files that need to be configured:

1. volumes/plugins/backends/saml2_backend.yaml
2. volumes/plugins/frontends/saml2_frontend.yaml
3. volumes/attributemaps/saml_uri.py
4. volumes/internal_attributes.yaml

If there is a need to add new attributes, follow the procedure in: Adding new attributes.


Useful commands
Script to generate metadata, run in container.

# . /opt/satosa/bin/activate

(satosa) # satosa-saml-metadata "${DATA_DIR}/proxy_conf.yaml" "${DATA_DIR}/metadata.key" "${DATA_DIR}/metadata.crt" --dir "${METADATA_DIR}" --split-frontend

abighe...